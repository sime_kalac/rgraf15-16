Monika Vukušić – Zadatak 4

Za regulaciju oblika putanje koriste se sljedeće tipke: 
•	'1' – odabir prve točke
•	'2' – odabir druge točke
•	'3' – odabir treće točke
•	'4' - odabir četvrte točke
Nakon odabira željene točke ista se može pomicati pomoću:
•	'h' – pomak u pozitivnom smjeru x osi
•	'f' – pomak u negativnom smjeru x osi
•	't' – pomak u pozivnom smjeru y osi
•	'g' - pomak u negativnom smjeru y osi
•	'u' – pomak u pozitivnom smjeru z osi
•	'j' – pomak u negativnom smjeru z osi
Bezierova krivulja definira se i regulira pomoću navedenih točaka te njihovih pozicija.
Odabir početne pozicije, a time i željenog smjera kretanja čelične kugle po putanji omogućen je pomoću slova 'i'.
Za pokretanje animacije potrebno je pritisnuti slovo 's', a ista se zaustavlja pritiskom na slovo 'e'. 
Kada čelična kugla dostigne krajnju točku putanje animacija se zaustavlja, a ukoliko se želi ponovno pokrenuti može se pritisnuti tipka 's' kako bi čelična kugla krenula natrag istom putanjom u drugom smjeru, ili pritiskom tipke 'i' ponovno mijenjati početnu poziciju.
Pritiskom tipke Esc omogućen je izlaz iz programa.
Sama implementacija programa opisana je komentarima u kodu.
Reference:
•	https://www.opengl.org/wiki/
•	http://www.videotutorialsrock.com/index.php
•	Vježbe i materijali s Mudri sustava
