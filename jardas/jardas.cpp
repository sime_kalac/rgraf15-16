#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>

bool wireFrameOn = false;
int Slices = 30;
float rotationSpeed = 0.8f;

//tocke konture case (x, y, z)
float kontura[] = {
	0, 		0,		0,
   -3, 		0,		0,
 -1.5, 		1,		0,
 -0.5, 		1,		0,
 -0.5, 		8,		0,
   -2, 		9,		0,
 -2.5, 	   10,		0,
   -3, 	   12,		0,
   -3, 	   14,		0,
   -2, 	   14,		0,
   -2, 	   12,		0,
 -1.5, 	   10,		0,
   -1, 		9,		0,
	0, 		9,		0
};

int konturaSize = sizeof(kontura)/sizeof(*kontura);
//pocetni kut
float angle = 0.0f;
//360 stupnjeva
float sliceAngle = 360/Slices;
//u radijane i u stupnjeve
float toRads = M_PI / 180;
float toDegs = 180 / M_PI;

void InitGL()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_POINTS);
	glPointSize(6.0f);
	float pos[] = {0.0f, 0.0f, 0.0f, 1.0f};
	glLightfv(GL_LIGHT0, GL_POSITION, pos);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -18.0f);

	glRotatef(angle, 0, 0, 1);
	glPushMatrix();

	glTranslatef(0, -7, 0);
	glPushMatrix();

	if(Slices > 2)
	{
		float thisSlice[konturaSize];
		float nextSlice[konturaSize];
		memcpy(thisSlice, kontura, sizeof(kontura));

		//klasicna dupla for petlja, i su podjeljci, j tocke u podjeljku
		for(int i = 0; i < Slices + 1; i++)
		{
			for(int j = 0; j < konturaSize; j += 3)
			{
				nextSlice[j+1] = kontura[j+1];
				nextSlice[j] = kontura[j] * cos(sliceAngle * i * toRads);
				nextSlice[j+2] = kontura[j] * sin(sliceAngle * i * toRads);
			}
			//wireframe on
			if (wireFrameOn)
			{
				//lines
				for(int j = 0; j < konturaSize-3; j+=3)
				{
					glBegin(GL_LINES);
					glColor3f(0.7, 0.7, 0.7);
					glVertex3f(thisSlice[j], thisSlice[j+1], thisSlice[j+2]);
					glVertex3f(thisSlice[j+3], thisSlice[j+4], thisSlice[j+5]);
					glVertex3f(nextSlice[j], nextSlice[j+1], nextSlice[j+2]);
					glVertex3f(nextSlice[j+3], nextSlice[j+4], nextSlice[j+5]);
					glEnd();

					glBegin(GL_LINES);
					glColor3f(0.7, 0.7, 0.7);
					glVertex3f(thisSlice[j], thisSlice[j+1], thisSlice[j+2]);
					glVertex3f(nextSlice[j], nextSlice[j+1], nextSlice[j+2]);
					glVertex3f(nextSlice[j+3], nextSlice[j+4], nextSlice[j+5]);
					glEnd();
				}
			}
			//wireframe off
			else
			{
				for(int j = 0; j < konturaSize-3; j+=3)
				{
					glBegin(GL_QUADS);
					glColor3f(0.7, 0.7, 0.7);
					glVertex3f(thisSlice[j], thisSlice[j+1], thisSlice[j+2]);
					glVertex3f(thisSlice[j+3], thisSlice[j+4], thisSlice[j+5]);
					glVertex3f(nextSlice[j], nextSlice[j+1], nextSlice[j+2]);
					glVertex3f(nextSlice[j+3], nextSlice[j+4], nextSlice[j+5]);
					glEnd();

					glBegin(GL_TRIANGLES);
					glColor3f(0.7, 0.7, 0.7);
					glVertex3f(thisSlice[j], thisSlice[j+1], thisSlice[j+2]);
					glVertex3f(nextSlice[j], nextSlice[j+1], nextSlice[j+2]);
					glVertex3f(nextSlice[j+3], nextSlice[j+4], nextSlice[j+5]);
					glEnd();
				}
			}
			memcpy(thisSlice, nextSlice, sizeof(kontura));
		}
	}
	glPopMatrix();
	glPopMatrix();

	//rotacija napred (+) ili nazad (-)
	angle -= rotationSpeed;

	glutSwapBuffers();
	glutPostRedisplay();
}

void Keyboard(unsigned char key, int x, int y) {

	switch (key)
	{
		//Esc
		case 27:
			exit(0);
		// S
		case 115:
			wireFrameOn = true;
			break;
		// W
		case 119:
			wireFrameOn = false;
			break;
	}
}

void Resize(int w, int h)
{
	if (h == 0) h = 1;
	GLfloat aspect = (GLfloat)w / (GLfloat)h;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90.0f, aspect, 1.0f, 100.0f);
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("čaša za vino");
	glutDisplayFunc(Display);
	glutKeyboardFunc(Keyboard);
	glutReshapeFunc(Resize);

	InitGL();
	glutMainLoop();

	return 0;
}
