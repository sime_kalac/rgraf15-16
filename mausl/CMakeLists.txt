cmake_minimum_required(VERSION 2.8)
# Project Name
PROJECT(Grafika_projekt)

find_package(GLUT REQUIRED)
include_directories(${GLUT_INCLUDE_DIRS})
link_directories(${GLUT_LIBRARY_DIRS})
add_definitions(${GLUT_DEFINITIONS})

find_package(OpenGL REQUIRED)
include_directories(${OpenGL_INCLUDE_DIRS})
link_directories(${OpenGL_LIBRARY_DIRS})
add_definitions(${OpenGL_DEFINITIONS})

add_executable(mausl mausl.cpp)
target_link_libraries(mausl  ${GLUT_LIBRARY} ${OPENGL_LIBRARY})
