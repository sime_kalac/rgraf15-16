Program iscrtava kuglu i krivulju po kojoj se kugla krece. Tipkom s pokrecemo animaciju. Tipkom e zaustavljamo (pauziramo) odnosno mijenjamo stanje varijable "int operate" (0/1). Tipkama 1 do 3 odabiremo tocku kojoj zelimo promijeniti poziciju. Tipkama a x s d mijenjamo poziciju odabranje točke u x i y smjeru. Tipkama l i r mijenjamo smjer. Pozicije točaka spremaju se u polje, mijenjaju s obzirom na tipke i iscrtavaju kontrolne točke, krivulju i kuglu u drawScene. Kugla je iscrtana pomocu funkcije koja kao argumente uzima broj meridijana i paralela.

izvori:

 BESIER-OVA KRIVULJA: template 3,5 labosi 6
 OSVJETLJENJE: http://www.codemiles.com/c-opengl-examples/adding-lighting-to-a-solid-sphere-t9125.html
 KUGLA: https://www.opengl.org
 TIPKE: 1,2,3 odabir točke 
	a x w d  position up,down, left,right
	r, l okret
	- krivulja i smjer mogu se mijenjati samo kad se kugla ne krece
	- http://nemesis.lonestar.org/reference/telecom/codes/ascii.html