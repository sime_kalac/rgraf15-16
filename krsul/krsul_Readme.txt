Kršul - 3. zadatak

Kod sam započela pisati u otvorenom projektu s 3. vježbi.

Kod je detaljnije objašnjen pomoću komentara u cpp datoteci.

Pri pokretanju programa, potrebno je pritisnuti slovo 's' za početak animacija. Ukoliko želimo napraviti pauzu pritisnemo 'x'.

Nakon startanja animacije biramo oko koje osi želimo rotirati koje klatno:
		- 'q' - rotiranje prvog klatna oko X osi
		- 'w' - rotiranje prvog klatna oko Y osi
		- 'e' - rotiranje prvog klatna oko Z osi
		- 'b' - rotiranje drugog klatna oko X osi
		- 'n' - rotiranje drugog klatna oko Y osi
		- 'm' - rotiranje drugog klatna oko Z osi


Linkovi koji su mi bili korisni za rješavanje zadaće:

	http://www.videotutorialsrock.com/index.php
	http://stackoverflow.com/questions/17518826/opengl-how-to-display-a-comet-and-path-traced-by-it
	https://www.opengl.org/sdk/docs/man2/xhtml/glGet.xml - i općenito na stranici www.opengl.org
	https://www.opengl.org/discussion_boards/showthread.php/127175-glGetFloatv%28GL_MODELVIEW_MATRIX-m%29

Nedostatak programa je crta koja spaja početnu i završnu točku crvenog traga kojeg ostavlja posljednja kugla.