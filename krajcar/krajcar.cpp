#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>

bool stanje = false; //pocetni kut i stanje
float angle = 0.0f;

GLfloat amb_light[]  = { 0.3, 0.3, 0.3, 1.0 }; //parametri osvjetljenja
GLfloat diff_light[]  = { 1.0, 1.0, 1.0, 1.0 };
GLfloat spec_light[] = { 1.0, 1.0, 1.0 };
GLfloat white_spec_mat[] = { 1.0, 1.0, 1.0 };
GLfloat shiny[]     = { 128 };



void light (void) { //standardne komande za osvjetljenje
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_SPECULAR, spec_light);
	glLightfv(GL_LIGHT0, GL_AMBIENT,  amb_light);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,  diff_light);


	glEnable(GL_COLOR_MATERIAL);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  white_spec_mat);
    	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shiny);  

}




// kontura case
float kontura[] = {
-2.5, 1, 1,
-0.5, 1, 0,
-0.5, 5, 0,
-2,9,0,
-2.5, 10, 0,
-2.6,11,0,
-2.7,13,0,
};




void display()
{
   
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClearColor(0.5, 0.5, 1, 0); //pozadina prozora
    glTranslatef(0.0f, -7, -13.0f); //translacija
    glRotatef(angle++, 0, 1, 0.2); //rotacija modela po y i z osi
    glLineWidth(2); //debljina crta kod zicanog prikaza


    
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //komande za transparentnost materijala
    glEnable(GL_BLEND);
    

    int vel = sizeof(kontura)/sizeof(*kontura);
    float a[vel];
    float b[vel];

    memcpy(a, kontura, sizeof(kontura));
    
    int br=30;
  
    
    //algoritam za crtanje modela
    for(int i = 0; i < br + 1; i++)
    {
      for(int j = 0; j < vel; j += 3)
      {
      b[j+1] = kontura[j+1];
      b[j] = kontura[j] * cos(360/30 * i * 3.14/180);
      b[j+2] = kontura[j] * sin(360/30 * i * 3.14/180);

      }
      if (stanje)
      {
        for(int j = 0; j < vel-3; j+=3)
            {
		  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //zicani model
                  glBegin(GL_TRIANGLE_STRIP);
                  glColor4f(0.5, 0.5, 0.5, 0.3);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(a[j+3], a[j+4], a[j+5]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glEnd();
             }
        }
        else
        {
          for(int j = 0; j < vel-3; j+=3)
              {
        
                  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //ispunjen model
                  glBegin(GL_TRIANGLE_STRIP);
                  glColor4f(0.5, 0.5, 0.5, 0.3);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(a[j+3], a[j+4], a[j+5]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glEnd();
                }
          }
          memcpy(a, b, sizeof(kontura));
          }

          
        glutSwapBuffers();   
      
      glutPostRedisplay();
  }

void keyboard(unsigned char key, int , int ) { //komande za unos s tipkovnice

        switch (key)
        {
                case 27:
                       exit(0);
               
                case 'w':
                        if(stanje)stanje=false;
                        else 
                        {
                        stanje=true;
                        }

        }
}

void reshape (int width, int height){
    glViewport (0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective (80, (GLfloat)width / (GLfloat)height, 1.0, 100.0);
    glMatrixMode (GL_MODELVIEW);
}

int main(int argc, char** argv)
{
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (400, 600);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Zadatak 2 - Filip Krajcar");
    
    glEnable(GL_DEPTH_TEST); // uklanja zakrivljene povrsine
    glShadeModel(GL_SMOOTH); // smooth povrsina
    
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutKeyboardFunc (keyboard);
    glutReshapeFunc (reshape); 
    light();

    glutMainLoop ();
    return 0;
}