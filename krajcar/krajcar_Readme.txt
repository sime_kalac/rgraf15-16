Filip Krajcar - Zadatak 2

Zadatak je kompajliran pomocu sljedece komande unutar terminala xubuntu-a: "g++ krajcar.cpp -lGL -lGLU -lglut -lm -o krajcar" te pokrenut sljedecom komandom: "./krajcar". Od radnog okruzenja je uglavnom bio koristen xubuntu 
uredivac teksa (Mousepad) te KDevelop. Vecina je komandi objasnjena u komentarima unutar koda, dok za neke klasicne komande komentari nisu niti bili pisani.

Komande programa: tipka "w" - mijenja iz zicanog u potpuni pregled modela case
				  tipka "ESC" - izlaz iz programa
				  
Od referenci i tutoriala je koristeno sljedece: 
https://www.cse.msu.edu/~cse872/tutorial3.html (tutorial3, tutorial4...)
http://www.swiftless.com/tutorials/opengl/material_lighting.html
https://www3.ntu.edu.sg/home/ehchua/programming/opengl/CG_Examples.html
https://www.opengl.org/archives/resources/ - velika vecina s navedenog linka
https://web.math.pmf.unizg.hr/nastava/CG/Pred_2.html - predavanja s PMF-a ZG

te naravno, kodovi s vjezbi.