// biblioteke
#include <iostream>
#include <GL/glut.h>
#include <GL/gl.h>
#include <Eigen/Dense>
#include <math.h>

using namespace Eigen;
using namespace std;

// prototipi funkcija
void initRenderer(void);
void resize(int, int);
void render(void);
void refreshAnimation(int);
void drawKugla(void);
int binoms ( int, int);
int factorial(int);
double polinom ( const int &n, const int &k, const double &t );
double valueOfFunctions ( const double &t, const VectorXd &v );
void drawControlPoints(void);
void generateBezierCurve(void);
void drawBezierCurve(void);
void keyWork(unsigned char, int, int);
void traice();





//globalne deklaracije varijabli
Vector4d kooX, kooY, kooZ; //z koordinata definirana iako je uvijek 0
Vector4d Xkoo, Ykoo, Zkoo;
VectorXd tX, tY, tZ;
VectorXd TS;
float dist;
float Cangle = -90;
int animation = 0;
int timeAnimation = 25;
int showBezier = 0;
int showBezierCurve = 0;
int animate = 1;
vector<float> putX;
vector<float> putY;
int stop = 0;


int main(int argc, char** argv) {
  
    kooX <<  -3.0, -1.55, -1.30, -1.35;
    kooY <<  2.90, 1.80, 0.8, -1.5;
    kooZ <<  0, 0, 0, 0;
    dist = 0.;
    generateBezierCurve();
    /*
    Xkoo <<  -1.35, -0.1, 1.0, 2;
    Ykoo <<  -1.5, 1.85, 1.8, -1.5;
    Zkoo <<  0, 0, 0, 0;
    dist = 0.;
    generateBezierCurve2();
    */
    //inicijalizacija GLUTa
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(400, 400); // postavi win32 prozor
   
    // napravi prozor
    glutCreateWindow("Projekt RG - Antonio Gizdulić");
    initRenderer(); //inicijalizacija iscrtavanja
   
    // postaviti ručice za funkcije koje manipuliraju scenom i prozorom
    glutDisplayFunc(render);
    glutReshapeFunc(resize);
   
    glutKeyboardFunc(keyWork);
    glutTimerFunc(timeAnimation, refreshAnimation, 0); //update gluta nakon 25ms
    //glShadeModel (/*GL_SMOOTH*/GL_FLAT);
    glShadeModel (GL_SMOOTH/*GL_FLAT*/);
    glutMainLoop(); // pokreće glavnu petlju
    return 0;  // simbolično, jer nikad se do nje neće doći
}

void initRenderer(void)
{
    // omogućuje renderiranje dubine tj. ako se objekt nađe ispred nekog drugog
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glClearColor(0.75f, 0.75f, 0.75f, 1.0f); //siva pozadina
     //postavljanje svjetla
    GLfloat light_diffuse[]={1.000f,1.000f,1.000f,1.0f};
    GLfloat light_position[]={3.0f,0.0f,0.0f,1.0f};
    //uporaba prozirnosti
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    //omogucavanje svjetla
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
}

// poziva se pri mijenjanju visine i širine prozora
void resize(int w, int h) {
    // postavljanje viewporta, kako pretvarati podatke u pixele
    glViewport(0, 0, w, h);
   
    glMatrixMode(GL_PROJECTION); // perspektiva kamere
   
    // postavljanje perspektive kamere
    glLoadIdentity(); // resetiraj kameru
    gluPerspective(45.0,                  // kut kamere
                   (double)w / (double)h, // aspect ratio ( podijeljena visina sa širinom )
                   1.0,                   // ono što je preblizu, ne renderiraj
                   200.0);                // ono što je predaleko, ne renderiraj
}

// iscrtava scenu
void render() {
    // očisti ekran od posljednjeg iscrtavanja
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   
    glMatrixMode(GL_MODELVIEW); // perspektiva za iscrtavanje
    glLoadIdentity(); // resetiraj perspektivu iscrtavanja 
    //traice();
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, -5.0f); //služi za translaciju točki
    	
	glPushMatrix();
    	glScalef(0.5,0.5,0.5);//veličina sfere
    
    	glColor3f(0.0f, 0.0f, 1.0f);//plava boja
    	drawKugla();
	traice();
    	glPopMatrix();
	glAccum(GL_ACCUM,0.5f);
	//glAccum(GL_RETURN,1.0f);

    
    if(showBezier){
    drawControlPoints();
    }
    if(showBezierCurve){
    drawBezierCurve();
    }
    glPopMatrix();
    glAccum(GL_RETURN,1.0f);
    glFlush();
    glutSwapBuffers(); // zamijeni back buffer sa front bufferom
}

void keyWork(unsigned char key, int, int){
  
  switch(key){
    //pokreće kretanje lopte
    case 's':
      animation = true;
      break;
      glutPostRedisplay();
    case 'S':
      animation = true;
      break;
      glutPostRedisplay();
    //zaustavlja kretanje lopte  
    case 'x':
      animation = false;
      break;
      glutPostRedisplay();
    case 'X':
      animation = false;
      break;
      glutPostRedisplay();
    //pokazuje kontrolne točke bezier spline  
    case 'b':
      showBezier = true;
      break;
      glutPostRedisplay();
    case 'B':
      showBezier = true;
      break;
      glutPostRedisplay();
    //sakriva kontrolne točke bezier spline  
    case 'm':
      showBezier = false;
      break;
      glutPostRedisplay();
    case 'M':
      showBezier = false;
      break;
      glutPostRedisplay();
    //pokazuje bezier spline po kojoj se kreće lopta  
    case 'c':
      showBezierCurve = true;
      break;
      glutPostRedisplay();
    case 'C':
      showBezierCurve = true;
      break;
      glutPostRedisplay();
    //sakriva bezier spline po kojoj se kreće lopta
    case 'z':
      showBezierCurve = false;
      break;
      glutPostRedisplay();
    case 'Z':
      showBezierCurve = true;
      break;
      glutPostRedisplay();
    //exit
    case 27:
      exit(0);
      break;
    default:
      break;
  }
}

//binomi -> n povrh k
int binoms (int n, int k){
  
    int b;
    int nF, kF, nkF;
    
    //ako je k veći od n ili manji od 0, rezultat je 0
    if(k>n || k<0) return 0;
    
    //ako su k i n jednaki, onda je rezultat binomnog koeficijena 1
    //ako je k = 0, također je rezultat jednak 1
    if(k==0 || k==n) return 1;
    
    //ako je k jednak 1, automatski je rezultat n
    if(k==1) return n;
    
    //ako je k > n-k, automatski se zna da je k = n-k
    if(k>(n-k)) k = n - k;

    b = 1;
    nF = factorial(n);
    kF = factorial(k);
    if (k != n-k) nkF = factorial(n-k);
    
    if(k != n-k) b = nF/(kF*nkF);
    if(k == n-k) b = nF/kF;

    return b;
}

int factorial(int a){
  int result = 1;
  int i;
  
  for(i=0; i<a; i++){
    result *= a;
    a -= 1;
  }
  
  return result;
}

//izračun polinoma za bezier spline
//pow funkcija lib math.h izračunava potenciju gdje je pow(baza, eksponent)
double polinom (const int &n, const int &k, const double &t){
  
    double result;
    result = pow ((1.-t),n-k) *pow (t,k);
    return result;
}

//vraća vrijednost za bezier spline
//izračunava formulu za bezier spline
double valueOfFunctions (const double &t, const VectorXd &v){
  
    int order = v.size()-1;
    int n, k;
    double result = 0;

    for(n=order, k=0; k<=n; k++){
      if (v( k )==0) continue;
        result += polinom ( n,k,t ) * binoms ( n,k )  * v ( k );
    }

    return result;
}

void drawControlPoints(){
  
    glPushMatrix();
    glColor3f(0.0f, 1.0f, 0.0f);
    glBegin(GL_LINE_STRIP); //spajanje točaka linijom
    glVertex3f(kooX( 0 ), kooY( 0 ), kooZ( 0 ));
    glVertex3f(kooX( 1 ), kooY( 1 ), kooZ( 1 ));
    glVertex3f(kooX( 2 ), kooY( 2 ), kooZ( 2 ));
    glVertex3f(kooX( 3 ), kooY( 3 ), kooZ( 3 ));
    
    glEnd();
    glPopMatrix();
}

//crtanje sfere, odnosno kugle
void drawKugla() {
    
      float x,y,z;
      int ver=50, hor=50;
    x = valueOfFunctions(dist, kooX);
    y = valueOfFunctions(dist, kooY);
    z = valueOfFunctions(dist, kooZ);
    glPushMatrix();
    glTranslatef(x,y,z);
    

    glBegin(GL_QUADS);
      //crta dvije nasuprotne četvrtine kruga
      int i, j;
      for(i = 0; i <= ver; i++) {
         double ver0 = M_PI * (-0.1 + (double) (i - 1) / ver);
         double z0  = sin(ver0);
         double zr0 =  cos(ver0);
    
         double ver1 = M_PI * (-0.1 + (double) i / ver);
         double z1 = sin(ver1);
         double zr1 = cos(ver1);
  
         glBegin(GL_QUAD_STRIP);
         for(j = 0; j <= hor; j++) {
             double hZontal = 1 * M_PI * (double) (j - 1) / hor;
             double x = cos(hZontal);
             double y = sin(hZontal);
    
             glNormal3f(x*zr0, y*zr0, z0);
             glVertex3f(x*zr0, y*zr0, z0);
             glNormal3f(x*zr1, y*zr1, z1);
             glVertex3f(x*zr1, y*zr1, z1);
         }
         glEnd();
	 
     }
     glPopMatrix();
}




//generiranje bezier krivulje
void generateBezierCurve(){
  
    int idx;
    
    TS = VectorXd::LinSpaced (21,0,1.);	
    tX.resize(TS.size());
    tY.resize(TS.size());
    tZ.resize(TS.size());

    for(idx=0; idx< TS.size(); ++idx){
        tX(idx) = valueOfFunctions(TS(idx), kooX);
        tY(idx) = valueOfFunctions(TS(idx), kooY);
        tZ(idx) = valueOfFunctions(TS(idx), kooZ);
    }
}



//crtanje bezier krivulje
void drawBezierCurve(){
  
    int i;
    glPushMatrix();
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_LINE_STRIP);
    for (i=0; i< TS.size(); ++i){ 
      glVertex3f(tX( i ), tY( i ), tZ( i )); 
      
    }
    glEnd();
    glPopMatrix();

}
//iscrtacanje traga
void traice() {
    
    float x = valueOfFunctions(dist, kooX);
    float y = valueOfFunctions(dist, kooY);
    putX.push_back (x);
    putY.push_back (y);
    
    // iscrtavanje putanje
    glBegin(GL_QUAD_STRIP);
    for (unsigned int i = 0; i < putX.size(); i++) {
	glColor3f(1.0f, 0.0f, 0.0f);
	glNormal3f(putX[i], putY[i], 0);
	glVertex3f(putX[i]+0.5f, putY[i]+0.5f, 0);
	glVertex3f(putX[i]+0.5f, putY[i]-0.5f, 0);
	glVertex3f(putX[i]-0.5f, putY[i]+0.5f, 0);
	glVertex3f(putX[i]-0.5f, putY[i]-0.5f, 0); 
	glVertex3f(putX[i]+0.5f, putY[i]+0.5f, 0); 
    }
    glEnd();
} 

//zapravo radi refresh distance
void refreshAnimation(int){
    stop++;
    if(animation){
    glutPostRedisplay();
    Cangle++;
    dist = 1 - Cangle/(0-90);
    if(Cangle > 0 ){Cangle = -90;}
    if(dist == 1){
      kooX <<  -1.35, -0.1, 1.5, 1.6;
      kooY <<  -1.5, 1.80, 1.75, -1.5;
      kooZ <<  0, 0, 0, 0;
      generateBezierCurve();
      dist = 0;
      if(Cangle == -90) animation = false; //pokušaj zaustavljanja u desnom kutu
    }
    }
    glutTimerFunc(timeAnimation, refreshAnimation, 0);
    glutPostRedisplay();
}



