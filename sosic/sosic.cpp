#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <GL/glut.h>

#define PI 3.14159265

static int isAnimate = 0; 
static int animationPeriod = 100; 
using namespace std;
static char theStringBuffer[10]; 
static long font = (long)GLUT_BITMAP_9_BY_15; 
static float t = 0.0; 
static float h = 0.5; 
static float v = 2.0; 
static float g = 1.0; 

void drawSphere() {
   	int i;
   	float x=0.0, y=0.0, radius=5;
	int triangleAmount = 200; 
	
	
	GLfloat twicePi = 2.0f * PI;
	
	glBegin(GL_TRIANGLE_FAN);
		glVertex2f(x, y);
		for(i = 0; i <= triangleAmount;i++) { 
			glColor3f(0.1, 0.1, 0.1);
			glVertex2f(
		            x + (radius * cos(i *  twicePi / triangleAmount)), 
					y + (radius * sin(i * twicePi / triangleAmount))
			);
		}
	glEnd();
}

void writeBitmapString(void *font, char *string)
{  
   char *c;

   for (c = string; *c != '\0'; c++) glutBitmapCharacter(font, *c);
}

void floatToString(char * destStr, int precision, float val) 
{
   sprintf(destStr,"%f",val);
   destStr[precision] = '\0';
}

void writeData(void)
{
   glColor3f(1.0, 1.0, 1.0);
   
   floatToString(theStringBuffer, 4, h);
   glRasterPos3f(-4.5, 4.5, -5.1);
  
   writeBitmapString((void*)font, theStringBuffer);
   
   floatToString(theStringBuffer, 4, v);
   glRasterPos3f(-4.5, 4.2, -5.1);
   
   writeBitmapString((void*)font, theStringBuffer);
}

void drawScene(void)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glClearColor(0.0, 0.0, 0.0, 1.0f);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   writeData();

   glTranslatef(-15.0, -15.0, -25.0);

   glTranslatef(h*t, v*t - (g/2.0)*t*t, 0.0);

   glColor3f(0.7, 0.7, 0.7);
   drawSphere();

   glutSwapBuffers();
}

void animate(int)
{
   if (isAnimate) 
   {
      t += 1.0;
   }
   glutTimerFunc(animationPeriod, animate, 1);
   glutPostRedisplay();
}

void setup(void) 
{
   GLfloat mat_ambient[] = {0.5, 0.5, 0.5, 0.5};
   GLfloat mat_specular[] = {1.0, 1.0, 1.0, 0.1};
   GLfloat mat_shininess[] = {70.0};
   GLfloat light_position[] = {2.0, 2.0, 5.0, 0.0};
   GLfloat model_ambient[] = {0.7, 0.5, 0.5, 1.0};
   glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
   glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
   glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
   glLightfv(GL_LIGHT0, GL_POSITION, light_position);
   glLightModelfv(GL_LIGHT_MODEL_AMBIENT, model_ambient);
   glClearColor(1.0, 1.0, 1.0, 1.0);
   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LESS);
   glEnable(GL_CULL_FACE);
   glEnable(GL_LIGHTING);
   glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_LIGHT0); 
}

void resize(int w, int h)
{
   glViewport(0, 0, (GLsizei)w, (GLsizei)h); 
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0);
   glMatrixMode(GL_MODELVIEW);
}

void keyInput(unsigned char key, int, int)
{
   switch(key) 
   {
      case 's':       
      case 'S':
            isAnimate = true;
            break;
            glutPostRedisplay();
	  case 'e': 
      case 'E':
			isAnimate = false;
			break;
			glutPostRedisplay();
		 break;
	  case 'r':
	  case 'R':
         isAnimate = 0;
		 t = 0.0;
         glutPostRedisplay();
		 break;
	  case 27:
         exit(0);
         break;
      default:
         break;
   }
}

void specialKeyInput(int key, int, int)
{
   if(key == GLUT_KEY_UP) v += 0.05;;
   if(key == GLUT_KEY_DOWN) if (v > 0.1) v -= 0.05;
   if(key == GLUT_KEY_RIGHT) h += 0.05; 
   if(key == GLUT_KEY_LEFT) if (h > 0.1) h -= 0.05;  
   glutPostRedisplay();
}

int main(int argc, char **argv) 
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize(700, 700);
   glutInitWindowPosition(0, 200); 
   glutCreateWindow ("Celicna kugla."); 
   setup(); 
   glutDisplayFunc(drawScene); 
   glutReshapeFunc(resize);  
   glutKeyboardFunc(keyInput);
   glutTimerFunc(5, animate, 1);
   glutSpecialFunc(specialKeyInput);
   glutMainLoop(); 

   return 0;  
}
