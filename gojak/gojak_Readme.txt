Loptica kreće padati iz gornjeg lijevog kuta, odbije se dvaput od "poda" i završi u donjem desnom kutu. Za sobom ostavlja trag crvene boje.
Animacija se pokreće tipkom 's', a završava u bilo kojem trenutku tipkom 'x', pri čemu loptica ostaje na mjestu na kojem je bila u trenutku pritiska tipke.

Korišten je template s laboratorijske vježbe, v05_template, kao i materijali s drugih vježbi i predavanja.
Usto, reference na internetu:
- http://www.opengl.org.ru/docs/pg/0208.html
- http://www.glprogramming.com/red/chapter05.html#name4
- http://stackoverflow.com/
