#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>

bool wire = false;

GLfloat gAmbientLight[]  = { 0.3, 0.3, 0.3, 1.0 };	// 
GLfloat gDiffuseLight[]  = { 1.0, 1.0, 1.0, 1.0 };	// 
GLfloat gSpecularLight[] = { 1.0, 1.0, 1.0 };		
GLfloat gWhiteSpecMate[] = { 1.0, 1.0, 1.0 };		// 
GLfloat gLightPosition[] = { 0.0, 5.0, 0.0, 1.0 };	// 
GLfloat gShining[]     = { 40 };			


float kontura[] = {
   	-2.5, 1, 1,
 	-0.5, 1, 0,
 	-0.5, 7, 0,
	-1.3, 7.7, 0,
	-1.78, 8.5, 0,
 	-2.3, 10, 0,
	-2.75, 12.5, 0,
	-3, 15, 0,
};


double angle = 0.0f;


void light (void) {
 
    glLightfv(GL_LIGHT0, GL_SPECULAR, gSpecularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT,  gAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  gDiffuseLight);
    glLightfv(GL_LIGHT0, GL_POSITION, gLightPosition);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.01);

    glEnable (GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  gWhiteSpecMate);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, gShining);

    glEnable(GL_DEPTH_TEST);

    glShadeModel(GL_SMOOTH);		
}

void display()
{
    glLineWidth(2);
    glClearColor(1.0f, 0.3, 0.3f, 1.0f);	

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(0.0f, -8.0f, -18.0f);
   
    angle= angle-1;

    glRotatef(angle, 0, 1, 0);
   

    int vel = sizeof(kontura)/sizeof(*kontura);

    float a[vel];
    float b[vel];
    
    
    memcpy(a, kontura, sizeof(kontura));
    
    int br = 120;
    
    for(int i = 0; i < br + 1; i++)
    {
      for(int j = 0; j < vel; j += 3)
      {
      b[j+1] = kontura[j+1];
      b[j] = kontura[j] * cos(720/60 * i * 3.14/180);
      b[j+2] = kontura[j] * sin(720/60 * i * 3.14/180);

      }
      if (wire)
      {
        for(int j = 0; j < vel-3; j+=3)
            {
                  glBegin(GL_LINES);
                  glColor4f(1.0, 1.0, 1.0, 0.1);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(a[j+3], a[j+4], a[j+5]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glEnd();
             }
        }
        else
        {
          for(int j = 0; j < vel-3; j+=3)
              {
                  glBegin(GL_QUADS);
                  glColor4f(1.0, 1.0, 1.0, 0.1);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(a[j+3], a[j+4], a[j+5]);
                  glEnd();

                  glBegin(GL_TRIANGLES);
                  glColor4f(1.0, 1.0, 1.0, 0.2);
                  glVertex3f(a[j], a[j+1], a[j+2]);
                  glVertex3f(b[j], b[j+1], b[j+2]);
                  glVertex3f(b[j+3], b[j+4], b[j+5]);
                  glEnd();
                }
          }
          memcpy(a, b, sizeof(kontura));
          }
          
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glEnable(GL_BLEND);          

      glutSwapBuffers();
      glutPostRedisplay();
  }

void keyboard(unsigned char key, int , int ) {

        switch (key)
        {
                case 27:
                       exit(0);
                case 's':
                        if(wire)wire=false;
                       	break;
                case 'w':
                        if(!wire)wire=true;
			break;
        }
}

void reshape (int width, int height){
    glViewport (0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective (60, (GLfloat)width / (GLfloat)height, 10.0, 100.0);
    glMatrixMode (GL_MODELVIEW);

	
}

int main(int argc, char** argv)
{
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (640, 480);
    glutInitWindowPosition (200, 200);
    glutCreateWindow ("Zadatak_2_tibor_jaklin");
    
    
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutKeyboardFunc (keyboard);
    glutReshapeFunc (reshape); 
    light();

    glutMainLoop ();
    return 0;
}