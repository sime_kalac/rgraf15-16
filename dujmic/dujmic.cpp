#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>

#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

//mogu probati i da napravim diferencijale, umjesto collision detection
long binomials ( long n, long k )
{
    // n && k >=0
    long i;
    long b;

    if ( 0 == k || n == k )
    {
        return 1;
    }

    if ( k > n )
    {
        return 0;
    }

    if ( k > ( n - k ) )
    {
        k = n - k;
    }

    if ( 1 == k )
    {
        return n;
    }

    b = 1;

    for ( i = 1; i <= k; ++i )
    {
        b *= ( n - ( k - i ) );
        if ( b < 0 ) return -1; /* ERR!!!! OVERFLOW */
        b /= i;
    }
    return b;
}

double polyterm ( const int &n, const int &k, const double &t )
{
    return pow ( ( 1.-t ),n-k ) *pow ( t,k );
}

double getValue ( const double &t, const VectorXd &v )
{
    int order = v.size()-1;
    double value = 0;
    for ( int n=order, k=0; k<=n; k++ )
    {
        if ( v ( k ) ==0 ) continue;
        value += binomials ( n,k ) * polyterm ( n,k,t ) * v ( k );
    }
    return value;
}

void changeSize ( int w, int h )
{
    if ( h == 0 )
    {
        h = 1;
    }
    float ratio = 1.0* w / h;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, w, h );
    gluPerspective ( 45,ratio,1,100 ); // view angle u y, aspect, near, far
}

Vector4d pt_x, pt_y, pt_z;
VectorXd t_x, t_y, t_z;
VectorXd ts;

void drawControlPoints()
{
    glPushMatrix();
    glColor3f ( 1.0f, 1.0f, 1.0f );
    //glBegin ( GL_LINE_STRIP );
    glVertex3f ( pt_x ( 0 ), pt_y ( 0 ), pt_z ( 0 ) );
    glVertex3f ( pt_x ( 1 ), pt_y ( 1 ), pt_z ( 1 ) );
    glVertex3f ( pt_x ( 2 ), pt_y ( 2 ), pt_z ( 2 ) );
    glVertex3f ( pt_x ( 3 ), pt_y ( 3 ), pt_z ( 3 ) );
    glEnd();
    glPopMatrix();
}

void generateBezierCurve()
{
    ts = VectorXd::LinSpaced ( 10,0,1 );
    t_x.resize ( ts.size() );
    t_y.resize ( ts.size() );
    t_z.resize ( ts.size() );

    for ( int idx=0; idx< ts.size(); ++idx )
    {
        t_x ( idx ) = getValue ( ts ( idx ), pt_x );
        t_y ( idx ) = getValue ( ts ( idx ), pt_y );
        t_z ( idx ) = getValue ( ts ( idx ), pt_z );
    }
}

// draw i kreira tocke i crta, trebalo bi raditi jednu stvar

void drawBezierCurve()
{
    glPushMatrix();
    glColor3f ( 0.5f, 0.0f, 0.0f );
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i )
    {
        glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) );
    }
    glEnd();
    glPopMatrix();
}

void drawBezierCurve1()
{
    glPushMatrix();
    glColor3f ( 0.5f, 0.0f, 0.0f );
    glBegin ( GL_LINE_STRIP );
    for ( int i=0; i< ts.size(); ++i )
    {
        glVertex3f ( t_x ( i ), t_y ( i ), t_z ( i ) );
    }
    glEnd();
    glPopMatrix();
}

Vector3d sphere_coo;

void drawSphere()
{
    glPushMatrix();
    glColor3f ( 0.0f, 0.0f, 1.0f );
    glPushMatrix();
        glTranslatef(sphere_coo(0), sphere_coo(1), sphere_coo(2));
        glutSolidSphere(0.5, 15,15);
    glPopMatrix();

}
//osjencavanje i sjencanje kugle
const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

float angle_0=-90;
float angle_n=0;

float angle = 90;

void update ( int /*value*/ )
{
    glutPostRedisplay();

    if(angle <0)
    {
        double t = (angle - angle_0)/(angle_n - angle_0);
        sphere_coo(0) = getValue(t, pt_x);
        sphere_coo(1) = getValue(t, pt_y);
        sphere_coo(2) = getValue(t, pt_z);
    } else {

        sphere_coo(0) = pt_x(0);
        sphere_coo(1) = pt_y(0);
        sphere_coo(2) = pt_z(0);
    }
    angle += (angle_n - angle_0)/fabs(angle_n-angle_0);
    //angle += 1;//povecavanjem ove vrijednosti krece se animacija
    if (angle > 0){angle=0;}
    //update glut-a nakon 25 ms
    glutTimerFunc ( 100, update, 0 );
}

void drawScene()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glMatrixMode ( GL_MODELVIEW ); // idemo u perspektivu
    glLoadIdentity(); // resetiranje

    gluLookAt ( 0.0,0.0,10.0, // camera
                4.0,-2.0,0.5, // where
                0.0f,1.0f,0.0f ); // up vector

    glPushMatrix();
       // glRotatef(angle, 0, 1, 0);

     glPushMatrix();
        glTranslatef(-pt_x(0), -pt_y(0), -pt_z(0));
            drawControlPoints();
            drawSphere();
            drawBezierCurve();
        glPopMatrix();
    glPopMatrix();

    glutSwapBuffers();
}

void drawScene1()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glMatrixMode ( GL_MODELVIEW ); // idemo u perspektivu
    glLoadIdentity(); // resetiranje

    gluLookAt ( 0.0,0.0,10.0, // camera
                4.0,-2.0,0.5, // where
                0.0f,1.0f,0.0f ); // up vector

    glPushMatrix();
       // glRotatef(angle, 0, 1, 0);

     glPushMatrix();
        glTranslatef(-pt_x(0), -pt_y(0), -pt_z(0));
            drawControlPoints();
            drawSphere();
            drawBezierCurve1();
        glPopMatrix();
    glPopMatrix();

    glutSwapBuffers();
}


//keyboard event

void keyboard(unsigned char key, int , int )
{
        switch (key)
        {
            case 27: // Escape key
                exit(0);
                break;
            case 's'://start animation
                glutDisplayFunc ( drawScene);
                glutDisplayFunc ( drawScene1);
                break;
            case 'x'://pause animation
               // glutDisplayFunc ( drawScene );
                break;
        }
}

static void display(void)
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3d(0,0,1);

    glPushMatrix();
        glTranslated(0.0,1.2,-6);
        glutSolidSphere(1,50,50);
    glPopMatrix();

    glutSwapBuffers();
}

int main ( int argc, char **argv )
{

    pt_x << -2.0, -1.0, 0.0, 0.0;//pocetne i krajnje tocke
    pt_y << 2.0, 1.0, 1.0, -2.0;
    pt_z << 0.0, 0, 0, 0;
    angle = angle_0;
    generateBezierCurve();

   // generateBezierCurve1();


    sphere_coo << pt_x(0), pt_y(0), pt_z(0);
    glutInit ( &argc, argv );
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize ( 600, 600 );

    glutCreateWindow ( "zad_1" );
    glutReshapeFunc ( changeSize );//velicina i prikaz objekata

    glutDisplayFunc(display);
        glClearColor(1,1,1,1);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        glEnable(GL_LIGHT0);
        glEnable(GL_NORMALIZE);
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_LIGHTING);

        glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
        glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
        glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);

        glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
        glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

    glutTimerFunc ( 25, update, 0 );

    glutKeyboardFunc(keyboard);

    glutMainLoop();

    return 0;
}