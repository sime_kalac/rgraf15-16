#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include<array>
 
#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;
 
float kut1=0.f;
float kut2=0.f;
float kut3=0.f;
float kut4=0.f;
float kut5=0.f;
float kut6=0.f;
float radius=0.03f;
 
int start;
int rotacija1Z;
int rotacija1X;
int rotacija1Y;
int rotacija2Z;
int rotacija2X;
int rotacija2Y;
 

vector<vector<float> > trag(1, vector<float>(16));
int counter = 0;
 
 
void update(int)
{
 
  if (start){
    if(rotacija1X){
      kut1 +=2.0f;
      if(kut1>360) kut1=kut1-360;
    }
    if(rotacija1Y){
      kut2 +=2.0f;
      if(kut2>360) kut2=kut2-360;
    }
    if(rotacija1Z){
      kut3 +=2.0f;      
      if(kut3>360) kut3=kut3-360;
    }
    if(rotacija2X){      
      kut4 +=2.0f;
      if(kut4>360) kut4=kut4-360;
    }
    if(rotacija2Y){
      kut5 +=2.0f;
      if(kut5>360) kut5=kut5-360;
    }
     if(rotacija2Z){
      kut6 +=2.0f;
      if(kut6>360) kut1=kut6-360;
    }
    }
    glutPostRedisplay();
    //update glut-a nakon 25 ms
    glutTimerFunc(25, update, 0);
}  
 
void izbornik (unsigned char key, int , int ){
   
  if(key==27){
      exit(0);
  }else if(key=='s'){
      start=1;    
    }else if(key=='x'){
      start=0;
    }else if(key=='q'){
      rotacija1X=1;
      rotacija1Y=0;
      rotacija1Z=0;
    }else if(key=='w'){
      rotacija1X=0;
      rotacija1Y=1;
      rotacija1Z=0;
    }else if(key=='e'){
      rotacija1X=0;
      rotacija1Y=0;
      rotacija1Z=1;
    }else if(key=='b'){
      rotacija2X=1;
      rotacija2Y=0;
      rotacija2Z=0;
    }else if(key=='n'){
      rotacija2X=0;
      rotacija2Y=1;
      rotacija2Z=0;
    }else if(key=='m'){
      rotacija2X=0;
      rotacija2Y=0;
      rotacija2Z=1;    
    }    
    }
 
void initRendering() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);    
}
 
void klatno1_Rotacija(){
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(kut3, 0.f, 0.f, 1.f );  
      glTranslatef(0.8f, -0.5f,0.f);
     
     
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(kut2, 0.f, 1.f, 0.f );  
      glTranslatef(0.8f, -0.5f,0.f);
     
     
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(kut1, 1.f, 0.f, 0.f );  
      glTranslatef(0.8f, -0.5f,0.f);
}
void klatno2_Rotacija(){
    glTranslatef(-0.8f, 0.f,0.f);
    glRotatef(kut6, 0.f, 0.f, 1.f );  
    glTranslatef(0.8f, 0.f,0.f);
     
   
    glTranslatef(-0.8f, 0.f,0.f);
    glRotatef(kut5, 0.f, 1.f, 0.f );  
    glTranslatef(0.8f, 0.f,0.f);
   
   
    glTranslatef(-0.8f, 0.f,0.f);
    glRotatef(kut4, 1.f, 0.f, 0.f );  
    glTranslatef(0.8f, 0.f,0.f);
       
 
 
}
void crtaj_trag()
{
 
   glColor3f(1.f, 0.f, 0.f);
   glBegin(GL_LINE_STRIP);
   for(int j = 0; j < counter; j++){
        glVertex3f(trag[j][12], trag[j][13], trag[j][14]);
    }
    glEnd();
}
 
 
void drawScene()
{
 
  GLfloat ambientColor[] = {1.f, 1.f, 1.f, 1.0f};
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
 
   
 
    glClearColor (0.0,0.0,0.0,1.0);
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
   
    GLUquadricObj *qobj;
    qobj = gluNewQuadric();
   
 

   
   
    glPushMatrix();
   
    glColor3f(0.8f,0.8f,0.8f);
    glRectf(-0.03,0.3f, 0.03f, 0.23f);
   
    glTranslatef(0.8f, -0.3f, 0.0f);
         
      klatno1_Rotacija();
     
      glColor3f(0.0f, 1.0f, 0.0f);
     
      glPushMatrix();
    glTranslatef(-0.8f, 0.5f,0.f);
    glutSolidSphere(radius, 100,100);
      glPopMatrix();
         
      glColor3f(0.0f, 0.5f, 1.0f);
     
      glPushMatrix();
    glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
    glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
    glTranslatef(0.f, -0.8f,-0.48f);
    gluCylinder(qobj,0.02f,0.02f,0.46f,100,100);
      glPopMatrix();
     
      glPushMatrix();
   
    klatno2_Rotacija();
     
    glColor3f(0.0f, 1.0f, 0.0f);
    glPushMatrix();
      glTranslatef(-0.8f,0.f,0.f);
      glutSolidSphere(radius, 100,100);
    glPopMatrix();
   
   
    glColor3f(1.0f, 0.25f, 0.0f);
    glPushMatrix();
      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      glTranslatef(0.f, 0.f,-0.78f);
      gluCylinder(qobj,0.02f,0.02f,0.46f,100,100);
    glPopMatrix();
   
    glColor3f(1.0f, 1.0f, 0.5f);
    glPushMatrix();
      glTranslatef(-0.305f,0.f,0.f);
      glutSolidSphere(radius, 100,100);
     
    vector<float> tmp (16);
        trag.push_back(tmp);
        glGetFloatv(GL_MODELVIEW_MATRIX, &trag[counter][0]);
    counter++;
   
    glPopMatrix();
      glPopMatrix();       
     
     glPopMatrix();

      
    crtaj_trag();   
       
    glutSwapBuffers();
}
 
const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 2.0f, 5.0f, 0.0f };
 
const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.f, 0.f, 0.f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };
 
int main(int argc, char **argv)
{
     
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1024, 768);
 
    glutCreateWindow("Toni Polonijo");
   
    glutDisplayFunc(drawScene);
    glutKeyboardFunc(izbornik);
    glutTimerFunc(25, update, 0);
   
   update(0);
   
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
 
 
    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
 
    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
       
    glutMainLoop();
 
    return 0;
}