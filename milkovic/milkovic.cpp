#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include<array>

#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

float angle1X=0.f;
float angle1Y=0.f;
float angle1Z=0.f;
float angle2X=0.f;
float angle2Y=0.f;
float angle2Z=0.f;


bool pokreni;
bool klatno1Z;
bool klatno1X;
bool klatno1Y;
bool klatno2Z;
bool klatno2X;
bool klatno2Y;


vector<vector<float> > trag(1, vector<float>(16));
int brojac = 0;


void update(int /*value*/)
{
  
  if (pokreni){
    if(klatno1Z){
      angle1Z +=2.0f;      
      if(angle1Z>360) angle1Z = 0;
     
    }
    
    if(klatno1Y){
      angle1Y +=2.0f;
      if(angle1Y>360) angle1Y = 0;
    }
    
    if(klatno1X){
      angle1X +=2.0f;
      if(angle1X>360) angle1X = 0;
    }
  
     if(klatno2Z){
      angle2Z +=2.0f;
      if(angle2Z>360) angle2Z = 0;
      
    }
    
    if(klatno2Y){
      angle2Y +=2.0f;
      if(angle2Y>360) angle2Y = 0;
      
    }
    
    if(klatno2X){      
      angle2X +=2.0f;
      if(angle2X>360) angle2X = 0;
      
  
    }   
    }
   
    glutPostRedisplay();
    glutTimerFunc(25, update, 0);
}   


void handleKeypress (unsigned char key, int /*x*/, int /*y*/){
    
  switch(key){
    case 27: //ESC
      exit(0);
    case 's':{
      pokreni=true;    
      update(0);
      break;
    }
    case 'x':{
      pokreni=false;
      update(0);
      break;
    }
    
    case'e':{
      klatno1Z=true;
      klatno1X=false;
      klatno1Y=false;
      update(0);
      break;
    }
    
    case'w':{
      klatno1Z=false;
      klatno1X=false;
      klatno1Y=true;
      update(0);
      break;
    }
    
    case'q':{
      klatno1Z=false;
      klatno1X=true;
      klatno1Y=false;
      update(0);
      break;
    }
    case 'm':{
      klatno2Z=true;
      klatno2X=false;
      klatno2Y=false;
      update(0);
      break;
    }
      
    case 'n':{
      klatno2Z=false;
      klatno2X=false;
      klatno2Y=true;
      update(0);
      break;
      
    }
    
    case 'b':{
      klatno2Z=false;
      klatno2X=true;
      klatno2Y=false;
      update(0);
      break;
      
    }      
    } 
}

void initRendering() {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING); 
	glEnable(GL_LIGHT0); 
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE); 
	glShadeModel(GL_SMOOTH); 
}

void handleResize (int w, int h){
  glViewport(0,0,w,h);
  
  glMatrixMode(GL_PROJECTION); 
  glLoadIdentity(); 
  gluPerspective(45.0, 
		(double)w / (double)h, 
		 1.0, 
		 200.0);
}

void crtajtrag()
{
  
   glColor3f(1.f, 0.f, 0.f);
   glBegin(GL_LINE_STRIP);
   for(int j = 0; j < brojac; j++){
        glVertex3f(trag[j][12], trag[j][13], trag[j][14]);
    }
    glEnd();
}


void drawKlatno1()
{
      GLUquadricObj *qobj;
    qobj = gluNewQuadric();
    
  glColor3f(1.0f, 1.0f, 0.0f);
      
      glPushMatrix();
	glTranslatef(-0.8f, 0.5f,0.f);
	glutSolidSphere(0.04f, 100,100);
	
      glPopMatrix(); 
          
      glColor3f(0.f,0.191f,0.255f);
      
      glPushMatrix();
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.f, -0.8f,-0.46f);
	gluCylinder(qobj,0.03f,0.03f,0.46f, 100, 100);
      glPopMatrix();
      
      glPushMatrix();
}
void rotateklatno1(){
   
  
      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(angle1Z, 0.f, 0.f, 1.f );  
      glTranslatef(0.8f, -0.5f,0.f);
      

      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(angle1Y, 0.f, 1.f, 0.f );  
      glTranslatef(0.8f, -0.5f,0.f);
      

      glTranslatef(-0.8f, 0.5f,0.f);
      glRotatef(angle1X, 1.f, 0.f, 0.f );  
      glTranslatef(0.8f, -0.5f,0.f);

}

void drawKlatno2()
{
	GLUquadricObj *qobj;
	qobj = gluNewQuadric();
    
	glColor3f(0.f, 0.f, 0.f);
	glPushMatrix();
	  glTranslatef(-0.8f,-0.04f,0.f);
	  glutSolidSphere(0.04f, 100,100);
	glPopMatrix(); 
	
	
	glColor3f(0.f, 0.255f, 0.127f);
	glPushMatrix();
	  glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	  glTranslatef(0.f, -0.04f,-0.755f);
	gluCylinder(qobj,0.03f,0.03f,0.46f, 100, 100);
	glPopMatrix();
	
	glColor3f(0.255f, 0.192f, 0.203f);
	glPushMatrix();
	glTranslatef(-0.255f,-0.04f,0.f);
	glutSolidSphere(0.04f, 100,100);
	
        vector<float> tmp (16);
        trag.push_back(tmp);
        glGetFloatv(GL_MODELVIEW_MATRIX, &trag[brojac][0]);
	brojac++;	

	glPopMatrix();
}

void rotateKlatno2()
{
 
	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angle2Z, 0.f, 0.f, 1.f );  
	glTranslatef(0.8f, 0.f,0.f);
      
	
	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angle2Y, 0.f, 1.f, 0.f );  
	glTranslatef(0.8f, 0.f,0.f);
	

	glTranslatef(-0.8f, 0.f,0.f);
	glRotatef(angle2X, 1.f, 0.f, 0.f );  
	glTranslatef(0.8f, 0.f,0.f);
	
}



void drawScene()
{
      GLfloat ambientColor[] = {1.f, 1.f, 1.f, 1.0f}; 
      glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
    
      glClearColor (1.0,1.0,1.0,1.0);
      glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glLoadIdentity();
 
      crtajtrag();
      
      glPushMatrix();
      glTranslatef(0.8f, -0.3f, 0.0f);
      
    
      rotateklatno1();     
      drawKlatno1(); 
      rotateKlatno2();
      drawKlatno2();
      glPopMatrix();	    
      
      glPopMatrix(); 
   
  
    
       
    glutSwapBuffers(); 
}

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 2.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.f, 0.f, 0.f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

int main(int argc, char **argv)
{
     
    glutInit(&argc, argv); 
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1000, 1000);

    glutCreateWindow("v3 - p1");
    
    glutDisplayFunc(drawScene);

    glutKeyboardFunc(handleKeypress);
    glutTimerFunc(25, update, 0);
    
   
     glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);


    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    
    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
    
   
    glutMainLoop();

    return 0;
}
