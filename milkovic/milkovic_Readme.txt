Vrijednosti za veličinu kuglice i klatna su izabrane proizvoljno, pomak klatna i kuglica je odabran da sve kuglice budu blizu klatnia te da se prva kuglica bude centrirana kad se pomiče klatno.
Neki od linkova koji su mi pomogli:
https://www.opengl.org/sdk/docs/man2/xhtml/glGet.xml 
http://stackoverflow.com/questions/16578027/rotating-an-object-around-a-fixed-point-in-opengl
http://www.glprogramming.com/red/chapter05.html
Prilikom pokretanja nije potrebno unositi nikakve argumente.
Program se koristi kako je napisanou opisu zadatka:
s – pokreće animaciju
x – kraj animacije
ESC – izlaz iz programa
q – rotacija prvog klatna oko x osi
w - rotacija prvog klatna oko y osi
e - rotacija prvog klatna oko z osi
b – rotacija drugog klatna oko x osi
n - rotacija drugog klatna oko y osi
m - rotacija drugog klatna oko z osi
Razvojna okolina: KDevelop 4 na Linux Mint 17.3