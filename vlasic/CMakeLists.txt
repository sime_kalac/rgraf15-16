cmake_minimum_required(VERSION 2.6)

find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)

set(EIGEN3_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/eigen-3.2.6/")

include_directories(${EIGEN3_INCLUDE_DIR})

add_executable(vlasic vlasic.cpp)
target_link_libraries(vlasic  ${GLUT_LIBRARY} ${OPENGL_LIBRARY})
