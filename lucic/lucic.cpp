#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include <GL/glu.h>

#include <iostream>
using namespace std;


void initGL() {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
    glClearDepth(1.0f);                   // Set background depth to farthest
    glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
    glDepthFunc(GL_LEQUAL);    // Set the type of depth-test
    glShadeModel(GL_SMOOTH);   // Enable smooth shading
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections
}

void changeSize ( int w, int h )
{

    if (h == 0) h = 1;
    GLfloat aspect = (GLfloat)w / (GLfloat)h;


    glViewport(0, 0, w, h);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();             // Reset

    gluPerspective(45.0f, aspect, 0.1f, 100.0f);
}



float dist;


int x_first;
int y_first;
int z_first; 
int x_second;
int y_second ;
int z_second;
void drawCyinder()
{
    GLUquadricObj *quadratic;
    quadratic = gluNewQuadric();



    
    gluCylinder(quadratic,0.7f,0.7f,6.0f,32,32);


}




float cube_angle=-180;
int x_angle = 90;
int x_angle_second = 0;
int y_angle = 0;
int y_angle_second = 90;
int z_angle = 0;
int z_angle_second = 0;
int arrayAngles[6];
int check = 1;
int angle;


bool x_reverse = false;
bool y_reverse = false;
bool z_reverse = false;
bool x_reverse_second = false;
bool y_reverse_second = false;
bool z_reverse_second = false;
int i,j,y =0;
void drawScene()
{
    glEnable(GL_LIGHT0);
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode ( GL_MODELVIEW ); // idemo u perspektivu
    glLoadIdentity(); // resetiranje

    gluLookAt ( 0.0,-20.0,10.0, // camera
                0.0,0.0,-1.0, // where
                0.0f,1.0f,0.0f ); // up vector

    glBegin(GL_QUADS);              // pravokutnik
    glColor4f(0.0f, 0.0f, 0.0f, 0.1f); // Dark Gray
    glVertex3f(-2.5f, 10.0f, 2.0f);
    glVertex3f(-2.5f, 7.1f, 1.0f);
    glVertex3f(2.5f, 7.1f, 1.0f);
    glVertex3f(2.5f, 10.0f, 2.0f);
    glEnd();
    glPushMatrix();
    glColor3f(0,1,0);
    glTranslatef(0, 7.8, 0);
    glutSolidSphere(-0.7f, 10, 10);

    glTranslatef(0, -1.5, 0);
    glPushMatrix();
    glColor3f( 0, 0, 0.8f ); // blue
    glRotatef(x_angle, 1, 0, 0);
    glRotatef(y_angle, 0, 1, 0);
    glRotatef(z_angle, 0, 0, 1);


    drawCyinder();

    glPushMatrix();

    glColor3f(0,1,0);
    glTranslatef(0, 0, 6.7);

    glutSolidSphere(-0.7f, 10, 10);
    glPushMatrix();
    glTranslatef(0.6, 0, 0);
    glColor3f( 0.7f, 0.1f, 0 ); // blue
    glRotatef(x_angle_second, 1, 0, 0);
    glRotatef(y_angle_second, 0, 1, 0);
    glRotatef(z_angle_second, 0, 0, 1);
    drawCyinder();

    glPushMatrix();
    glTranslatef(0, 0, 6.8);
    glColor3f(1.0, 1.0, 0);
    glutSolidSphere(-0.7f, 10, 10);
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();



    glPopMatrix();

    glFlush();
    glutSwapBuffers();
}

int continu = 0;

void keyboard(unsigned char key, int x, int y)
{

    switch (key) {
    case 27:  // escape key
        exit(0);
        break;
    case 115: // s
        continu = 1;
        break;
    case 120: //x
        continu = 0;
        break;
    case 113:
        x_first = 1;
        y_first = 0;
        z_first = 0;

        break;
    case 119:
        y_first = 1;
        x_first = 0;
        z_first = 0;
        break;
    case 101:
        z_first = 1;
        y_first = 0;
        x_first = 0;
        break;
    case 98:
        x_second = 1;
        y_second = 0;
        z_second = 0;
        break;
    case 110:
        y_second = 1;
        x_second = 0;
        z_second = 0;
        break;
    case 109:
        z_second = 1;
        x_second = 0;
        y_second = 0;
    default:
        break;
    }
}

float add_angle(float angle, bool reverse)
{
    if (reverse){
        return angle--;
    } else {
        return angle++;

    }
}


void update ( int /*value*/ )
{
    if (continu){
        glutPostRedisplay();
        cube_angle += 1;
        if (cube_angle > 0){cube_angle = -180;}
        if (x_first){
            if (x_angle==240){
                x_reverse = true;
            } else if (x_angle==30){
                x_reverse = false;
            }
            if (x_reverse){
                x_angle--;
            } else {
                x_angle++;
            }
        } else if (y_first){
            if (y_angle==90){
                y_reverse = true;
            } else if (y_angle==-90) {
                y_reverse = false;
            }
            if (y_reverse){
                y_angle--;
            } else {
                y_angle++;
            }
        }
        else if  (z_first){
            if (z_angle==180){
                z_reverse = true;
            } else if (z_angle==0) {
                z_reverse = false;
            }
            if (z_reverse){
                z_angle--;
            } else {
                z_angle++;
            }
        }

        if (x_second){
            i++;
            if (x_angle_second==240){
                j++;
                x_reverse_second = true;
            } else if (x_angle_second==-30){
                x_reverse_second = false;
                y++;
            }
            if (x_reverse_second){
                i++;
                x_angle_second--;
            } else {
                y++;
                x_angle_second++;
            }
        }else if (y_second){
            if (y_angle_second==90){
                y_reverse_second = true;
            } else if (y_angle_second==-90){
                y_reverse_second = false;
            }
            if (y_reverse_second){
                y_angle_second--;
            } else {
                y_angle_second++;
            }
        }else if (z_second){
            if (z_angle_second==180){
                z_reverse_second = true;
            } else if (z_angle_second==0){
                z_reverse_second = false;
            }
            if (z_reverse_second){
                z_angle_second--;
            } else {
                z_angle_second++;
            }
        }

        //update glut-a nakon 25 ms
    }
    glutTimerFunc ( 25, update, 0 );
    

}

void initLights()
{
    glEnable(GL_DEPTH_TEST);

    GLfloat ambientColor[] = {0.4f, 0.4f, 0.4f, 1.0f}; //Color(0.2, 0.2, 0.2)
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
    
    GLfloat lightColor0[] = {0.8f, 0.8f, 0.8f, 1.0f}; //Color (0.5, 0.5, 0.5)
    GLfloat lightPos0[] = {-1.0f, -1.0f, -1.0f, 1.0f}; //Positioned at (4, 0, 8)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);//Add directed light
    GLfloat lightColor1[] = {1.0f, 0.2f, 0.2f, 1.0f}; //Color (0.5, 0.2, 0.2)
    //Coming from the direction (-1, 0.5, 0.5)
    GLfloat lightPos1[] = {7.0f, 7.0f, 0.0f, 0.0f};
    glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor1);
    glLightfv(GL_LIGHT1, GL_POSITION, lightPos1);
    
    
    

    GLfloat light_position[] = {0.0, 0.0, 0.0, 1.0};    // Pozicija svijetla
    glShadeModel(GL_SMOOTH);    // Shading model

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);  // Postavke svijetla LIGHT0

    //glEnable(GL_LIGHTING);  // Omogučavanje svijetla
    glEnable(GL_LIGHT0);    // Omogučavanje svijetla LIGHT0

    glEnable(GL_COLOR_MATERIAL);    //Omogučavanje boje*/

    glEnable(GL_BLEND); // Omogučavanje prozirnosti, blend
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // Blend funkcija*/
}

int main ( int argc, char **argv )
{
    glutInit ( &argc, argv );
    
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize ( 800, 800 );
    
    glutCreateWindow ( "Zadaca" );
    glutReshapeFunc ( changeSize );
    glutDisplayFunc ( drawScene );
    glutTimerFunc ( 25, update, 0 );
    //glShadeModel (/*GL_SMOOTH*/GL_FLAT);
    glShadeModel (GL_FLAT);
    glutKeyboardFunc(keyboard);
    initGL();
    initLights();
    glutMainLoop();

    return 0;
}











