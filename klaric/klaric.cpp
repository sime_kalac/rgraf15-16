#include <iostream>
#include <GL/glut.h>
#include <math.h>
 
GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0};
GLfloat blackAmbientLight[] = {0.8, 0.8, 0.8};
GLfloat whiteDiffuseLight[] = {1.0, 1.0, 1.0};
GLfloat whiteSpecularMaterial[] = {10.0, 10.0, 10.0};
GLfloat mShininess[] = {130};
 
 
float z = 0.0;
float x_bottom1 = -1, y_bottom1 = 0;
float x_bottom2 = -1, y_bottom2 = 0;
float x_bottom3 = -0, y_bottom3 = 0;
float x_height = -0.05, y_height = 2;
float x_bezierO1 = -0, y_bezierO1 = 2;
float x_bezierO2 = -1, y_bezierO2 = 2;
float x_bezierO3 = -1, y_bezierO3 = 3.5;
float x_bezierO4 = -0.5, y_bezierO4 = 4.5;
float x_bezierI1 = -0.5, y_bezierI1 = 4.5;
float x_bezierI2 = -1, y_bezierI2 = 3.5;
float x_bezierI3 = -1, y_bezierI3 = 2;
float x_bezierI4 = -0, y_bezierI4 = 2;
float x_top1 = -0.8, x_top2 = -0.7, y_top = 4.5;
 
const int num2D = 4;
const int num3Dx = 4;
const int num3Dy = 4;
 
GLfloat angle = 0.0f;
GLfloat rot_angle = 90;
GLfloat rot_angleb = 70;
bool flagOut = 0;
 
 
GLfloat controlpts2DO[num2D][3] = {
   {x_bezierO1, y_bezierO1, z},
   {x_bezierO2, y_bezierO2, z},
   {x_bezierO3, y_bezierO3, z},
   {x_bezierO4, y_bezierO4, z},
};
 
GLfloat controlpts2DI[num2D][3] = {
   {x_bezierI1, y_bezierI1, z},
   {x_bezierI2, y_bezierI2, z},
   {x_bezierI3, y_bezierI3, z},
   {x_bezierI4, y_bezierI4, z},
};
 
GLfloat controlpts3DO[num3Dx][num3Dy][3] = {
  {{x_bezierO1, y_bezierO1, z},
   {x_bezierO2, y_bezierO2, z},
   {x_bezierO3, y_bezierO3, z},
   {x_bezierO4, y_bezierO4, z}},
 
};
 
GLfloat controlpts3DI[num3Dx][num3Dy][3] = {
  {{x_bezierI1, y_bezierI1, z},
   {x_bezierI2, y_bezierI2, z},
   {x_bezierI3, y_bezierI3, z},
   {x_bezierI4, y_bezierI4, z}}, 
};
 
int numdrawsegs = 10;        
const int startwinsize = 400;
 
void init2DBezier (GLfloat controlpts[num2D][3]);
void init3DBezier (GLfloat controlpts[num3Dx][num3Dy][3]);
void init (void);
void light (void);
void draw2DBezierCurve (void);
void draw3DBezierCurve (void);
void drawGlassOutline (void);
void drawGlass (void);
void display (void);
void keyboard (unsigned char key, int x, int y);
void reshape (int width, int height);
 
int main (int argc, char **argv) {
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (800, 800);
    glutInitWindowPosition (500, 500);
    glutCreateWindow ("Staklena casa");
    init();
   
    glutDisplayFunc (display);
    glutIdleFunc (display);
    glutKeyboardFunc (keyboard);
    glutReshapeFunc (reshape);
   
    glutMainLoop ();
    return 0;
}
 
void init (void) {
    glEnable (GL_DEPTH_TEST);
    glEnable (GL_LIGHTING);
    glEnable (GL_LIGHT0);
   
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}
 
void keyboard (unsigned char key, int /*x*/, int /*y*/){
    if (key=='w'){
      glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    }
    if (key=='s'){
      glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }
   
    if(flagOut) flagOut = 0;
    else flagOut = 1;
 
    if (key==27){
      exit (0);
    }
}
 
void light (void) {
    glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
   
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, whiteSpecularMaterial);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);
}
 
void reshape (int width, int height){
    glViewport (0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective (60, (GLfloat)width / (GLfloat)height, 1.0, 100.0);
    glMatrixMode (GL_MODELVIEW);
}
 
void display (void) {
    glClearColor(0.f, 0.5f, 1.f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
   
    light();
   
    glTranslatef(0,0,-10);
    glRotatef(30,1.0f,0.0f,0.0f);
   
    glRotatef(angle,0.0f,0.0f,1.0f);
   
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
   
    if(flagOut) drawGlassOutline();
    else drawGlass();
   
    glutSwapBuffers();
   
    angle++;
}
 
void drawGlass (void) {
    float yangle=0;
   
    glLineWidth(1);
    glColor4f(0.5, 0.5, 0.5, 0.3);
   
   
    glBegin(GL_TRIANGLE_FAN);
      glVertex3f(0.0, 0.0, z);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
    glVertex3f(x_bottom1*cos(yangle * 3.14 / 180), y_bottom1, x_bottom1*sin(yangle * 3.14 / 180));
      }
    glEnd();
   
    glBegin(GL_TRIANGLE_STRIP);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
    glVertex3f(x_bottom1*cos(yangle * 3.14 / 180), y_bottom1, x_bottom1*sin(yangle * 3.14 / 180));
    glVertex3f(x_bottom2*cos(yangle * 3.14 / 180), y_bottom2, x_bottom2*sin(yangle * 3.14 / 180));
      }
    glEnd();
   
    glBegin(GL_TRIANGLE_STRIP);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
    glVertex3f(x_bottom2*cos(yangle * 3.14 / 180), y_bottom2, x_bottom2*sin(yangle * 3.14 / 180));
    glVertex3f(x_bottom3*cos(yangle * 3.14 / 180), y_bottom3, x_bottom3*sin(yangle * 3.14 / 180));
      }
    glEnd();
   
    glBegin(GL_TRIANGLE_STRIP);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
    glVertex3f(x_bottom3*cos(yangle * 3.14 / 180), y_bottom3, x_bottom3*sin(yangle * 3.14 / 180));
    glVertex3f(x_height*cos(yangle * 3.14 / 180), y_height, x_height*sin(yangle * 3.14 / 180));
      }
    glEnd();
   
   
    glBegin(GL_TRIANGLE_FAN);
      glVertex3f(0.0, y_height, z);
      for(yangle=0; yangle<=360; yangle+=rot_angle){
    glVertex3f(x_height*cos(yangle * 3.14 / 180), y_height, x_height*sin(yangle * 3.14 / 180));
      }
    glEnd();
   
   
   for(yangle=0; yangle<360; yangle+=rot_angle){
 
      init3DBezier(controlpts3DO);
      draw3DBezierCurve();
     
 
      init3DBezier(controlpts3DI);
      draw3DBezierCurve();
     
      glRotatef(rot_angle,0.0f,1.0f,0.0f);
    }
   
   
      glBegin(GL_TRIANGLE_STRIP);
      for(yangle=0; yangle<=360; yangle+=10){
 
      }
    glEnd();
}
 
void drawGlassOutline (void) {
 
    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0, 0.0, z);
    glVertex3f(x_bottom1, y_bottom1, z);
    glVertex3f(x_bottom2, y_bottom2, z);
    glVertex3f(x_bottom3, y_bottom3, z);
 
    glVertex3f(x_height, y_height, z);
 
    glEnd();
   
    init2DBezier(controlpts2DO);
    draw2DBezierCurve();
   
    init2DBezier(controlpts2DI);
    draw2DBezierCurve();
   
    glBegin(GL_LINES);
 
    glEnd();
}
 
void draw2DBezierCurve (void) {
    glEvalMesh1(GL_LINE, 0, numdrawsegs);
}
 
void draw3DBezierCurve (void) {
    glEvalMesh2(GL_FILL, 0, numdrawsegs, 0, numdrawsegs);
}
 
 
void init2DBezier (GLfloat controlpts[num2D][3]) {
    glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, num2D, &controlpts[0][0]);
    glEnable(GL_MAP1_VERTEX_3);
    glMapGrid1d(numdrawsegs, 0.0, 1.0);
}
 
void init3DBezier (GLfloat controlpts[num3Dx][num3Dy][3]) {
    glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, num3Dx, 0, 1, 12, num3Dy, &controlpts[0][0][0]);
    glEnable(GL_MAP2_VERTEX_3);
    glMapGrid2f(numdrawsegs, 0.0, 1.0, numdrawsegs, 0.0, 1.0);
}