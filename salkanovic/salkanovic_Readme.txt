Opis
Program je napisan u C++ programskom jeziku. Kompajliran je koristeci Terminal u Linuxu, naredbom
g++ salkanovic.cpp -lGL -lGLU -lglut -o salkanovic te potom pokrenut s ./salkanovic

Kontrole:
----------
Tipka "s" -> pokretanje animacije
Tipka "e" -> zaustavljanje animacije
Strelice lijevo, desno, gore, dolje -> promjena smjera i oblika putanje po kojoj se giba kugla
Tipka "ESC" -> izlaz iz programa

Program inicijalno prikazuje (celicnu) kuglu koja nije nacrtana pomocu gluSphere. Umjesto toga, kugla je definirana koristeci ikosaedarsku podjelu (icosahedron subdivision). Ideja je da se pocne definirajuci ikosaeader, pravilni poliedar koji za stranice ima dvadeset jednakostraničnih trokuta.
Nadalje, potrebno je uzastopno podijeliti svaku trokutastu stranicu u manje trokute u svakoj fazi. Broj faza ce odrediti koliko ce se trokuta generirati, te ce naposljetku rezultirati zatvaranjem mreze u potrebnu kuglu.
Uz samu kuglu, prilikom pokretanja programa vidljiva je Bezierova krivulja obojana u zutu boju. Krivulja je nacrtana koristeci cetiri kontrolne tocke definirane s float control_points[4][3]. Promjena ovih vrijednosti rezultirat ce promjenom oblika krivulje po kojoj se giba kugla.
Ukoliko se korisniku cini da je animacija prespora, odnosno da se kugla presporo krece po krivulji, omoguceno je ubrzanje animacije promjenom vrijednosti varijable float tInc. 

Pritiskom na tipku "s" na tipkovnici animacija se pokrece te kugla mjenja svoju pocetnu poziciju
pritom prateci putanju koja je definirana gore navedenom Bezierovom krivuljom. Vrijednost spomenute varijable tInc postavlja se na 0.0001f. Ukoliko korisnik u bilo kojem trenutku stisne tipku "e" animacija se odmah zaustavlja te kugla prestaje mjenjati svoj smjer. Vrijednost tInc tada je postavljena na 0.0f. Tijekom animacije, kombinacijom strelica (lijevo, desno, gore, dolje) na tipkovnici omogucena je regulacija smjera i oblika putanje, odnosno same Bezierove krivulje. 
U trenutku kada kugla dostigne krajnu tocku krivulje, zaustavlja svoju kretnju sto ujedno i oznacava kraj animacije. Korisniku tada jedino preostaje pritisak na "ESC" koja zatvara program.
Takoder, u bilo kojem trenutku izvodjenja animacije, omogucen je izlazak iz programa koristeci navedenu tipku "ESC".

Iz razloga sto su komentari vezani uz kôd, odnosno pojedine funkcije prilozeni u .cpp datoteci, isti nece biti postavljeni ovdje radi izbjegavanja nepotrebnog dupliciranja te lakseg pracenja kôda.

Reference:
https://open.gl/
http://www.opengl-tutorial.org/  - osnove OpenGL-a
http://www.opengl.org.ru/docs/pg/0208.html - ikosaedar
- primjeri s vjezbi i predavanja
http://stackoverflow.com/ - forum koristen za sve ostalo

